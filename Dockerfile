FROM openjdk:8-jre-alpine

EXPOSE 8080

COPY ./target/CoreProject-*.jar /usr/app/
WORKDIR /usr/app

CMD java -jar CoreProject-*.jar

